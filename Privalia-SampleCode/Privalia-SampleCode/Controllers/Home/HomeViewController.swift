//
//  HomeViewController.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/14/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    private var selectedMedia: MediaContent?
    private var lastPageLoaded = 0
    private var lastSearchPageLoaded = 0
    private var allMediaContent:   [MediaContent] = []
    private var searchMediaContent:[MediaContent] = []
    private var isloadingMore = false
    private var isSearchLoadingMode = false
    private var isSearching = false
    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Load more media queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    private let kSegueToMediaDetails = "GoToMediaDetails"
    private let collectionSource = MediaContentCollectionViewSource()
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let flowLayout = mediaCollectionView.collectionViewLayout as? UICollectionViewFlowLayout{
        flowLayout.estimatedItemSize = CGSize(width:320, height:270)
        }
        mediaCollectionView.dataSource  = collectionSource
        mediaCollectionView.delegate    = collectionSource
        collectionSource.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        definesPresentationContext = true
        
        loadMoreMedia()//TODO: Call if netowrk status change and reset page count to 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let mediaDetails as MediaDetailsViewcoViewController:
            mediaDetails.media = selectedMedia
        default:
            break
        }
    }
    
    @IBAction func didTapInhabitantsCollection(_ sender: Any) {
        searchController.searchBar.resignFirstResponder()
    }
}


//MARK: - InhavitantsCollectionSource Delegates
extension HomeViewController: MediaContentCollectionViewSourceDelegate{
    func didSelect(item: MediaContent) {
        selectedMedia = item
        performSegue(withIdentifier: kSegueToMediaDetails, sender: self)
    }
    
    func didScrollToAlmostBottom() {
        if isSearching{
            
        }else{
            if !isloadingMore{
                isloadingMore = true
                loadMoreMedia()
                print("Load More")
            }
        }
    }
}

extension HomeViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else{return}
        
        if searchText.isEmpty{//Not searching
            isSearching = false
            if lastPageLoaded < 1{
                mediaCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                loadMoreMedia()
            }else{
                collectionSource.allMediaContent = allMediaContent
                mediaCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                mediaCollectionView.reloadData()
            }
        }else{//Searching
            queue.cancelAllOperations()
            isSearching = true
            lastSearchPageLoaded = 0
            searchMediaContent.removeAll()
            mediaCollectionView.reloadData()
            mediaCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            searchMedia(withText: searchText)
        }
    }
}

//MARK: - Private Methods
extension HomeViewController{
    private func loadMoreMedia(){
        queue.addOperation { [weak self] in
            guard let `self` = self else{return}
            _ = TheMovieDBNetworkManager.shared.getMovies(forPage: self.lastPageLoaded+1)
            .done{ [weak self] pageMediaContet in
                guard let `self` = self else{return}
                if !self.isSearching{
                    self.lastPageLoaded += 1
                    self.allMediaContent.append(contentsOf: pageMediaContet)
                    self.collectionSource.allMediaContent = self.allMediaContent
                    self.mediaCollectionView.reloadData()
                    self.isloadingMore = false
                }
            }
        }
    }
    
    private func searchMedia(withText text:String){
        queue.addOperation { [weak self] in
            guard let `self` = self else{return}
            self.lastSearchPageLoaded += 1
            _ = TheMovieDBNetworkManager.shared.searchMovies(withText: text, forPage: self.lastSearchPageLoaded)
            .done{ [weak self] pageMediaContet in
                guard let `self` = self else{return}
                if self.isSearching{
                    self.searchMediaContent.append(contentsOf: pageMediaContet)
                    self.collectionSource.allMediaContent = self.searchMediaContent
                    self.mediaCollectionView.reloadData()
                    self.isSearchLoadingMode = false
                }
            }
        }
    }
}

