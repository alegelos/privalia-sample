//
//  MediaContentCollectionViewSource.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/16/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit

protocol  MediaContentCollectionViewSourceDelegate: class{
    func didSelect(item: MediaContent)
    func didScrollToAlmostBottom()
}

class MediaContentCollectionViewSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let kMediaCellIdentifier = "MediaCell"
    var allMediaContent:[MediaContent] = []
    weak var delegate: MediaContentCollectionViewSourceDelegate?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMediaContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMediaCellIdentifier, for: indexPath) as? MediaContentCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.loadCell(withMedia: allMediaContent[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(item: allMediaContent[indexPath.item])
    }
}

extension MediaContentCollectionViewSource: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isScrollContentCloseToMax(contentSizeHeight: scrollView.contentSize.height, contentOffSetY: scrollView.contentOffset.y){
            delegate?.didScrollToAlmostBottom()
        }
    }
}

//MARK: - Private Methods
extension MediaContentCollectionViewSource{
    private func isScrollContentCloseToMax(contentSizeHeight: CGFloat, contentOffSetY: CGFloat) -> Bool{
        return contentSizeHeight - contentOffSetY < 1000 && contentSizeHeight > 0
    }
}
