//
//  MediaContentCollectionViewCell.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/15/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit

class MediaContentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var overView: UILabel!
    @IBOutlet weak var imageLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var votes: UILabel!
    
    func loadCell(withMedia media:MediaContent) {
        name.text     = media.original_title
        overView.text = media.overview
        year.text     = DateUtils.getYear(fromDate: media.release_date)
        if let votesAverage = media.vote_average{
            votes.text = String(format: "%.1f", votesAverage)
        }
        
        image.image = nil
        if let imagePath = media.poster_path{
            loadImage(fromPath: imagePath)
        }
    }
}

//MARK: - Private Methods
extension MediaContentCollectionViewCell{
    private func loadImage(fromPath path: String){
        imageLoadingIndicator.startAnimating()
        TheMovieDBNetworkManager.shared.getImage(forImageType: .poster_sizes, forPath: path, desireSize: "w300")
        .done{ [weak self] args in
            guard args.requestURL.contains(path) else{return}
            self?.image.image = args.image
        }.ensure { [weak self] in
            self?.imageLoadingIndicator.stopAnimating()
        }.catch{ [weak self] error in
            self?.image.image = #imageLiteral(resourceName: "NoCoverImage")
        }
    }
}
