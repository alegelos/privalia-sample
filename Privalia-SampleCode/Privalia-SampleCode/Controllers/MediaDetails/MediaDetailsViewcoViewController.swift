//
//  MediaDetailsViewcoViewController.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/17/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit

class MediaDetailsViewcoViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var averageVotes: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var overView: UILabel!
    @IBOutlet weak var imageLoadingIndicator: UIActivityIndicatorView!
    
    var media: MediaContent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text     = media?.original_title
        overView.text = media?.overview
        year.text     = DateUtils.getYear(fromDate: media?.release_date)
//        genre.text    = media?.genre_ids //TODO get gener by id
        if let votesAverage = media?.vote_average{
            averageVotes.text = String(format: "%.1f", votesAverage)
        }
        
        image.image = nil
        if let imagePath = media?.poster_path{
            loadImage(fromPath: imagePath)
        }
    }
}

//MARK: - Private Methods
extension MediaDetailsViewcoViewController{
    private func loadImage(fromPath path: String){
        imageLoadingIndicator.startAnimating()
        let desireSize = "w"+String(describing: Int(view.bounds.size.width))
        TheMovieDBNetworkManager.shared.getImage(forImageType: .poster_sizes, forPath: path, desireSize: desireSize)
            .done{ [weak self] args in
                guard args.requestURL.contains(path) else{return}
                self?.image.image = args.image
            }.ensure { [weak self] in
                self?.imageLoadingIndicator.stopAnimating()
            }.catch{ [weak self] error in
                self?.image.image = #imageLiteral(resourceName: "NoCoverImage")
        }
    }
}
