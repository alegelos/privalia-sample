//
//  TheMovieDBNetworkManager.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/14/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

class TheMovieDBNetworkManager {
    
    static let shared = TheMovieDBNetworkManager()
    private init(){}
    
    enum AccessType:String{
        case publicAccess, withOauth
    }
    
    private var theMovieDBConfig: TheMovieDBConfig?
    private let imageCache = NSCache<NSString, UIImage>()
    
    private var activeNetworkProcess = 0 {
        didSet{
            UIApplication.shared.isNetworkActivityIndicatorVisible = activeNetworkProcess > 0
        }
    }
    
    func getMovies(forPage page:Int) -> Promise<[MediaContent]>{
        return firstly{
            TheMovieDBNetworkUtils.getEndPointWithPromise(forService: .getMovies)
        }.then{ url -> Promise<URL> in
            self.addPageNumber(toUrl: url, page: page)
        }.then{ url -> Promise<URL> in
            self.getEndPointURLForAccessType(forURL: url, forAccessType: .publicAccess)
        }.then{ [weak self] url -> Promise<(json: Any, response: PMKAlamofireDataResponse)> in
            self?.updateNetworkActivityCounter(numberOfProcess: 1)
            return Alamofire.request(url, method: .get, headers: self?.getHeaders(forAccessType: .publicAccess)).responseJSON()
        }.then{ (args) -> Promise<[MediaContent]> in
            let (value, _) = args
            return self.getMedia(fromJson: value)
        }.ensure { [weak self] in
            self?.updateNetworkActivityCounter(numberOfProcess: -1)
        }
    }
    
    func searchMovies(withText searchText:String, forPage page:Int) -> Promise<[MediaContent]>{
        return firstly{
            TheMovieDBNetworkUtils.getEndPointWithPromise(forService: .searchMovie)
        }.then{ url -> Promise<URL> in
            self.getEndPointURLForAccessType(forURL: url, forAccessType: .publicAccess)
        }.then{ url -> Promise<URL> in
            self.addSearchTex(toUrl: url, searchText: searchText)
        }.then{ url -> Promise<URL> in
            self.addPageNumberOldVersion(toUrl: url, page: page)
        }.then{ [weak self] url -> Promise<(json: Any, response: PMKAlamofireDataResponse)> in
            self?.updateNetworkActivityCounter(numberOfProcess: 1)
            return Alamofire.request(url, method: .get, headers: self?.getHeaders(forAccessType: .publicAccess)).responseJSON()
        }.then{ (args) -> Promise<[MediaContent]> in
            let (value, _) = args
            return self.getMedia(fromJson: value)
        }.ensure { [weak self] in
            self?.updateNetworkActivityCounter(numberOfProcess: -1)
        }
    }
    
    
    //DesireSize: expected image size, if no such size, return the next bigger one, if no bigger one, return the previous bigger one
    //Sizes examples: "w300", "original" or "h300"
    //h for horizontal
    //w for width
    func getImage(forImageType imagetype:TheMovieDBConfig.ImageTypes, forPath path:String, desireSize size:String) -> Promise<(requestURL: String, image: UIImage)> {
        return firstly{
            loadConfigIfneeded()
        }.then{
            self.getImageUrl(forImageType: imagetype, forPath: path, withSize: size)
        }.then{ [unowned self] url -> Promise<(requestURL: String, image: UIImage)> in
            if let cachedImage = self.imageCache.object(forKey: url.absoluteString as NSString) {
                return Promise { seal in seal.fulfill((url.absoluteString, cachedImage))}
            }
            return self.getImage(fromUrl: url)
        }
    }

    
    @discardableResult private func doRequest(forService service: TheMovieDBNetworkUtils.ServiceType, accessType: AccessType, method: HTTPMethod) -> Promise<(URLRequest, HTTPURLResponse, Data)> {
        return firstly{
            TheMovieDBNetworkUtils.getEndPointWithPromise(forService: service)
            }.then{ url -> Promise<URL> in
                self.getEndPointURLForAccessType(forURL: url, forAccessType: accessType)
            }.then{ [weak self] url -> Promise<(URLRequest, HTTPURLResponse, Data)> in
                self?.updateNetworkActivityCounter(numberOfProcess: 1)
                return Alamofire.request(url, method: method, parameters: nil, headers: self?.getHeaders(forAccessType: accessType)).response(.promise)//TODO add parameters
            }.ensure { [weak self] in
                self?.updateNetworkActivityCounter(numberOfProcess: -1)
        }
    }
}

//MARK: - Private Services Methods
extension TheMovieDBNetworkManager{
    
    private func getHeaders(forAccessType accessType: AccessType) -> HTTPHeaders{
        switch accessType {
        case .publicAccess:
            return ["Content-Type" : "application/json;charset=utf-8"]
        case .withOauth:
            return ["Content-Type" : "application/json;charset=utf-8", "Authorization" : "ACCESSTOKEN"]//TODO ACCESS
        }
    }
    
    private func getEndPointURLForAccessType(forURL url:URL ,forAccessType accessType: AccessType) -> Promise<URL>{
        return Promise<URL>{ seal in
            switch accessType {
            case .publicAccess:
                guard let url = addPublicAccess(toUrl: url) else{
                    seal.reject(NetworkError.failTogetUrlRequest)
                    return
                }
                seal.fulfill(url)
            case .withOauth:
                seal.fulfill(url)
            }
        }
    }
    
    private func addPublicAccess(toUrl url:URL)->URL?{
        guard let apiKey = TheMovieDBNetworkUtils.getApiKey(),
              let url = URL(string: url.absoluteString + "?api_key=" + apiKey) else{
                return nil
        }
        return url
    }
    
    //Version 4
    private func addPageNumber(toUrl url:URL, page: Int) -> Promise<URL>{
        return Promise<URL>{ seal in
            guard let url = URL(string: url.absoluteString + "/\(page)") else {
                seal.reject(NetworkError.failTogetUrlRequest)
                return
            }
            seal.fulfill(url)
        }
    }
    
    //Version 3
    private func addPageNumberOldVersion(toUrl url:URL, page: Int) -> Promise<URL>{
        return Promise<URL>{ seal in
            guard let url = URL(string: url.absoluteString + "&page=\(page)") else {
                seal.reject(NetworkError.failTogetUrlRequest)
                return
            }
            seal.fulfill(url)
        }
    }
    
    private func addSearchTex(toUrl url:URL, searchText text: String) -> Promise<URL>{
        return Promise<URL>{ seal in
            guard let url = URL(string: url.absoluteString + "&query=\(text)") else {
                seal.reject(NetworkError.failTogetUrlRequest)
                return
            }
            seal.fulfill(url)
        }
    }
    
    private func getMedia(fromJson json:Any)->Promise<[MediaContent]>{
        return Promise{ seal in
            guard let pageDic = json as? [String:Any],
                  let pageMediaContentDic = pageDic["results"] as? [[String:Any]] else{
                seal.reject(NetworkError.failToParseResponse)
                return
            }

            var pageMediaContent = Array<MediaContent>()

            for media in pageMediaContentDic{
                pageMediaContent.append(MediaContent(withDic: media))
            }
            seal.fulfill(pageMediaContent)
        }
    }
}

//MARK: - Private Images Methods
extension TheMovieDBNetworkManager{
    private func getImage(fromUrl url: URL) -> Promise<(requestURL: String, image: UIImage)>{
        return Promise{ seal in
            if let cachedImage = self.imageCache.object(forKey: url.absoluteString as NSString) {
                seal.fulfill((url.absoluteString, cachedImage))
            }
            
            self.updateNetworkActivityCounter(numberOfProcess: 1)
            _ = Alamofire.request(url).responseData()
            .done{ args -> () in
                let (data, response) = args
                guard let image = self.getImageFromResponse(data: data, rps: response) else{
                    seal.reject(NetworkError.failToGetImageFromData)
                    return
                }
                seal.fulfill(image)
            }.ensure { [unowned self] in
                self.updateNetworkActivityCounter(numberOfProcess: -1)
            }
        }
    }
    
    private func getImageUrl(forImageType imageType: TheMovieDBConfig.ImageTypes, forPath path:String, withSize size: String) -> Promise<URL>{
        return Promise{ seal in
            guard let baseUrl = theMovieDBConfig?.baseURL,
                let size = theMovieDBConfig?.getBestSize(forImageType: imageType, desireSize: size),
                let url = URL(string: baseUrl + "/\(size)" + path) else{
                    seal.reject(NetworkError.failTogetUrlRequest)
                    return
            }
            seal.fulfill(url)
        }
    }
    
    private func getImageFromResponse(data: Data, rps: PMKAlamofireDataResponse) -> (requestURL: String, image: UIImage)?{
        guard let image = UIImage(data: data),
              let responseURL = rps.response?.url?.absoluteString else{
                return nil
        }
        self.imageCache.setObject(image, forKey: responseURL as NSString)
        return (responseURL, image)
    }
    
    private func loadConfigIfneeded() -> Promise<Void>{
        return Promise{ seal in
            guard theMovieDBConfig == nil else{
                seal.fulfill(())
                return
            }
            
            _ = firstly{ () -> Promise<(json: Any, response: PMKAlamofireDataResponse)> in
                getConfig()
            }.done{ [weak self] (args) -> Void in
                let (json, _) = args
                guard let config = self?.getConfig(fromJson: json) else{
                    seal.reject(NetworkError.failToParseResponse)
                    return
                }
                self?.theMovieDBConfig = TheMovieDBConfig(withDic: config)
                seal.fulfill(())
            }
        }
    }
    
    private func getConfig() -> Promise<(json: Any, response: PMKAlamofireDataResponse)>{
        return firstly{
            TheMovieDBNetworkUtils.getEndPointWithPromise(forService: .getConfig)
            }.then{ url -> Promise<URL> in
                self.getEndPointURLForAccessType(forURL: url, forAccessType: .publicAccess)
            }.then{ [weak self] url -> Promise<(json: Any, response: PMKAlamofireDataResponse)> in
                self?.updateNetworkActivityCounter(numberOfProcess: 1)
                return Alamofire.request(url, method: .get, headers: self?.getHeaders(forAccessType: .publicAccess)).responseJSON()
            }.ensure{ [weak self] in
                self?.updateNetworkActivityCounter(numberOfProcess: -1)
        }
    }
    
    private func getConfig(fromJson json:Any)-> Dictionary<String, Any>?{
        guard let config = json as? [String:Any],
            let imagesConfig = config["images"] as? Dictionary<String, Any> else{
                return nil
        }
        return imagesConfig
    }
}

//MARK: - Private Visuable Methods
extension TheMovieDBNetworkManager{
    private func updateNetworkActivityCounter(numberOfProcess amount:Int){
        activeNetworkProcess = activeNetworkProcess + amount
    }
}
