//
//  MediaContent.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/15/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

struct MediaContent {
    
    let poster_path:       String?
    let media_type:        String?
    let backdrop_path:     String?
    let genre_ids:         [Int]?
    let vote_count:        Int?
    let overview:          String?
    let original_title:    String?
    let popularity:        Float?
    let release_date:      Date?
    let id:                Int?
    let video:             Bool?
    let original_language: String?
    let vote_average:      Float?
    let title:             String?
    let adult:             Bool?
    
    init(withDic dic:Dictionary<String,Any>) {
        poster_path       = dic["poster_path"]       as? String
        media_type        = dic["media_type"]        as? String
        backdrop_path     = dic["backdrop_path"]     as? String
        genre_ids         = dic["genre_ids"]         as? [Int]
        vote_count        = dic["vote_count"]        as? Int
        overview          = dic["overview"]          as? String
        original_title    = dic["original_title"]    as? String
        popularity        = dic["popularity"]        as? Float
        id                = dic["id"]                as? Int
        video             = dic["video"]             as? Bool
        original_language = dic["original_language"] as? String
        vote_average      = dic["vote_average"]      as? Float
        title             = dic["title"]             as? String
        adult             = dic["adult"]             as? Bool
        
        release_date = DateUtils.getDate(fromString: dic["release_date"] as? String)
    }
    
    
    //TODO Whats is this
    //    ▿ value : 3 elements
    //    - 0 : 18
    //    - 1 : 35
    //    - 2 : 10749
}
