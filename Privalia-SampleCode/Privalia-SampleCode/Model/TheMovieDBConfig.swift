//
//  TheMovieDBConfig.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/16/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

struct TheMovieDBConfig {
    enum ImageTypes:String {
        case backdrop_sizes, logo_sizes, poster_sizes, profile_sizes, still_sizes
        
        static let allValues = [backdrop_sizes, logo_sizes, poster_sizes, profile_sizes, still_sizes] //TODO Update to CaseIterable in Swift 4.2
    }
    
    private enum measureType:String{
        case w, h
    }
    
    var imageSizes: Dictionary<String,Array<(Int,Int)>>
    var baseURL:String?
    
    init(withDic dic:Dictionary<String,Any>) {
        imageSizes = Dictionary<String,Array<(Int,Int)>>()
        baseURL    = dic["secure_base_url"] as? String
        
        for imageType in ImageTypes.allValues{
            guard let values = dic[imageType.rawValue] as? Array<String> else{debugPrintError(); continue}
            imageSizes[imageType.rawValue] = getImagesSizes(forSizes: values)
        }
    }
    
    func getBestSize(forImageType image: ImageTypes, desireSize size:String) -> String?{
        guard let sizes = imageSizes[image.rawValue] else{return nil}
        
        var result:String?
        switch size{
        case "original":
            if isOriginalSizeAvaiable(sizes: sizes){result = "original"}
        default:
            result = getBestSize(forSize: size, fromSizes: sizes)
        }
        return result
    }
}

//MARK: - Private Methods
extension TheMovieDBConfig{
    private func getImagesSizes(forSizes sizes: Array<String>) -> Array<(Int,Int)>{
        var result = Array<(Int, Int)>()
        for size in sizes{
            switch size{
            case "original":
                result.append((-1,-1))
            case let str where str.contains("w"):
                guard let size = trimImageSize(fromString: size) else{debugPrintError();continue}
                result.append((size,0))
            case let str where str.contains("h"):
                guard let size = trimImageSize(fromString: size) else{debugPrintError();continue}
                result.append((0,size))
            default:
                debugPrintError()
            }
        }
        return result
    }
    
    private func trimImageSize(fromString stringValue: String) -> Int?{
        return Int(stringValue.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890.").inverted))
    }
    
    private func getBestSize(forSize size:String, fromSizes sizes:Array<(Int, Int)>) -> String?{
        var bestSize:String?
        switch size{
            case "original":
                if isOriginalSizeAvaiable(sizes: sizes){
                    bestSize = "original"
                }
            case let str where str.contains("w"):
                bestSize = getBetterWidth(forSize: size, fromValues: sizes)
            case let str where str.contains("h"):
                bestSize = getBetterHeight(forSize: size, fromValues: sizes)
            default:
                break
        }
        return bestSize
    }
    
    private func getBetterWidth(forSize size: String, fromValues sizes:Array<(Int, Int)>) -> String?{
        guard let sizeValue = trimImageSize(fromString: size) else{debugPrintError();return nil}
        var bestValue:Int?
        for size in sizes{
            bestValue = size.0
            if size.0 >= sizeValue {
                return "w"+String(size.0)
            }
        }
        if isOriginalSizeAvaiable(sizes: sizes){return "original"}
        if let bestValue = bestValue{ return "w"+String(describing: bestValue)}
        return nil
    }
        
    private func getBetterHeight(forSize size: String, fromValues sizes:Array<(Int, Int)>) -> String?{
        guard let sizeValue = trimImageSize(fromString: size) else{debugPrintError();return nil}
        var bestValue:Int?
        for size in sizes{
            bestValue = size.1
            if size.1 >= sizeValue{
                return "h"+String(size.1)
            }
        }
        if isOriginalSizeAvaiable(sizes: sizes){return "original"}
        if let bestValue = bestValue{ return "h"+String(describing: bestValue)}
        return nil
    }
    
    private func isOriginalSizeAvaiable(sizes: Array<(Int,Int)>) -> Bool{
        return sizes.contains(where: {$0 == (-1, -1)})
    }
    
    private func debugPrintError(){
        debugPrint("Some image sizes could not be read")
    }
}
