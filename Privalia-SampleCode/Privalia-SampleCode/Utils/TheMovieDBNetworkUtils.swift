//
//  TheMovieDBNetworkUtils.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/14/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import PromiseKit

struct TheMovieDBNetworkUtils {
    
    enum ServiceType:String{
        case baseUrl, getMovies, getConfig, searchMovie
    }

    enum ServicesConfig:String{
        case apiKey
    }
    
    static func getEndPoint(forService service: ServiceType) -> URL? {
        guard let servicesDic = PListUtils.getDic(fromPList: .TheMovieDBServices),
              let baseURL = servicesDic[ServiceType.baseUrl.rawValue] as? String else {
                return nil
        }
        
        switch service {
        case .baseUrl:
            return URL(string: baseURL)
        default:
            guard let servicePath = servicesDic[service.rawValue] as? String else{ return nil}
            return URL(string: baseURL + servicePath)
        }
    }
    
    static func getApiKey() -> String?{
        guard let servicesDic = PListUtils.getDic(fromPList: .TheMovieDBServices),
              let apiKey = servicesDic[ServicesConfig.apiKey.rawValue] as? String else {
                return nil
        }
        return apiKey
    }
    
    static func getEndPointWithPromise(forService service:ServiceType) -> Promise<URL>{
        return Promise{ seal in
            guard let url:URL = getEndPoint(forService: service) else{
                seal.reject(NetworkError.failTogetUrlRequest)
                return
            }
            
            seal.fulfill(url)
        }
    }
}
