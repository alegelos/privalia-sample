//
//  Errors.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/14/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case failTogetUrlRequest, failToParseResponse, failToGetImageFromData
}
