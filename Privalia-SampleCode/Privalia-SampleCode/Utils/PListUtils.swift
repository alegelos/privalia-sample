//
//  PListUtils.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/14/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

enum PListName:String {
    case TheMovieDBServices
}

struct PListUtils {
    static func getDic(fromPList pList: PListName) -> Dictionary<String, Any>?{
        guard let dicPath = Bundle.main.path(forResource: pList.rawValue, ofType: "plist") else{
            return nil
        }
        
        return NSDictionary(contentsOfFile: dicPath) as? Dictionary<String, Any>
    }
}
