//
//  DateUtils.swift
//  Privalia-SampleCode
//
//  Created by Vicky on 4/16/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

struct DateUtils {
    static func getDate(fromString stringDate:String?) -> Date?{
        guard let stringDate = stringDate else{return nil}
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.date(from: stringDate)
    }

    static func getYear(fromDate date:Date?) -> String?{
        guard let date = date else{return nil}
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        
        return dateFormatter.string(from: date)
    }
}
